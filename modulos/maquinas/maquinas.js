
function cargarTablaMaquinas() {
    tablaMaquinas = "";
    listadoMaquinas.forEach(function (doc) {
        numero = doc.num
        if (parseInt(doc.num) < 10) {
            numero = "0" + doc.num
        }
        balance = parseInt(doc.entrada) - parseInt(doc.salida)

        tablaMaquinas += `
    <tr>
    <td style="width:16.6%;"> <button type="button" class="btn btn-primary" onclick="maquina.cargarModificar('`+ doc.id + `')">
  `+ numero + `
    <span class="sr-only">unread messages</span>
  </button></td>
    <td style="width:16.6%;">`+ doc.multiplicador + `</td>
    <td style="width:16.6%;">`+ doc.entrada + `</td>
    <td style="width:16.6%;">`+ doc.salida + `</td>
    <td style="width:16.6%;">`+ balance + `</td>
    <td style="width:16.6%;">`+ doc.local + `</td>
    </tr>
    `
    });

    $("#cuerpoMaquinas").html(tablaMaquinas)

}

function filtroLocal(local) {
    if(local==""){
        cargarTablaMaquinas()
    }else{
        maquinasFiltradas = listadoMaquinas.filter(function (el) {

            return el.local.toLowerCase().startsWith(local.toLowerCase());
        })
    
        tablaMaquinas=""
        maquinasFiltradas.forEach(function (doc) {
            numero = doc.num
            if (parseInt(doc.num) < 10) {
                numero = "0" + doc.num
            }
            balance = parseInt(doc.entrada) - parseInt(doc.salida)
    
            tablaMaquinas += `
        <tr>
        <td style="width:20%;"> <button type="button" class="btn btn-primary" onclick="maquina.cargarModificar('`+ doc.id + `')">
      `+ numero + `
        <span class="sr-only">unread messages</span>
      </button></td>
        <td style="width:20%;">`+ doc.entrada + `</td>
        <td style="width:20%;">`+ doc.salida + `</td>
        <td style="width:20%;">`+ balance + `</td>
        <td style="width:20%;">`+ doc.local + `</td>
     
        </tr>
        `
        });
    
        $("#cuerpoMaquinas").html(tablaMaquinas)
    }
   
}

