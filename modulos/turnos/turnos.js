



function iniciarRutaTurnos() {

    db.collection("turnos/").where("jornada", "==", listadoJornadas[0].id).onSnapshot(function (querySnapshot) {
        listadoTurnos = [];
        querySnapshot.forEach(function (doc) {
            listadoTurnos.push({
                id: doc.id,
                apertura: doc.data().apertura,
                cierre: doc.data().cierre,
                jornada: doc.data.jornada,
                estado: doc.data().estado,
                cuadratura: doc.data().cuadratura
            });


        });
        //cargamos la tabla de turnos
        cargarListadoTurnos();
        cargarTurnoActual();
    })
}

function cargarListadoTurnos() {
    contenidoLista = "";
    contTurno = 0;
    listadoTurnos.forEach(function (doc) {
        contTurno++;
        if (doc.estado == true) {
            boton = `
    <span class="badge badge-primary badge-pill" onclick="turnos.cargarAbierto('`+ doc.id + `')">abierto </span>
    `
        } else {
            boton = `
    <span class="badge badge-primary badge-pill" onclick="turnos.cargarCerrado('`+ doc.id + `')">cerrado </span>
    `
        }

        contenidoLista += `
    <tr>
    <td style="width:20%;"><button class='btn btn-info'> `+ contTurno + `</button></td>
    <td style="width:20%;">`+ doc.apertura + `</td>
    <td style="width:20%;">`+ doc.cierre + `</td>
    <td style="width:20%;">   `+ boton + `   </td>
    </tr>
    `
    });

    $("#listadoTurnos").html(contenidoLista)

}
function cargarTurnoActual() {
    turnActiv = listadoTurnos.filter(function (el) {
        return el.estado == true
    })
    if (turnActiv < 1) {
        localStorage.turnoActual = ""
    } else {

        localStorage.turnoActual = turnActiv[0].id
    }
}