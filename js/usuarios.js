let Usuario = {
    nombre: "",
    apellido: "",
    tipo: "",
    id: "",
    correo: ""

}

function logear(correo, pass) {
    var control = 0;
    firebase.auth().signInWithEmailAndPassword(correo, pass).catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        control = 1;
        console.log(error.code)
        if (error.code == "auth/user-not-found") {
            alert("Usuario no encontrado")
        } else {
            if (error.code == "auth/wrong-password") {
                alert("Pass no valida")
            } else { }
        }
        // ...
    });
}
firebase.auth().onAuthStateChanged(function (user) {
    console.log(user)
    if (user) {
        var test = cargarUsuario(user.uid)
    } else {
        cargar("app", "login", "login");
    }
});



function cargarUsuario(usuario) {
    var docRef = db.collection("usuarios").doc(usuario);

    docRef.get().then(function (doc) {
        if (doc.exists) {
          
            Usuario.nombre = doc.data().nombre
            Usuario.apellido = doc.data().apellido
            Usuario.tipo = doc.data().tipo
            
            Usuario.id = doc.data().id
            Usuario.correo = doc.data().correo
            if (Usuario.tipo == "administrador") {
                console.log("Estoy cargando " + Usuario.tipo);
           
                cargar("nav", "navbar", "navbar")
                cargar("sideNav", "navbar", "sideNav");
                cargar("app", "diaActual", "listado");
            }
            if (Usuario.tipo == "trabajador") {
           
                console.log("Estoy cargando " + Usuario.tipo);
                Usuario.local = doc.data().local;
                cargar("nav", "navbar", "navbar")
                cargar("sideNav", "navbar", "sideNav");
                cargar("app", "jornadas", "jornada");
            }
        
        } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
        }

    }).catch(function (error) {
        console.log("Error getting document:", error);
    });

}

function cerrarsession() {
    Swal.fire({
        title: 'Desea cerrar la sesion?',
        text: "está seguro?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'OK'
      }).then((result) => {
        if (result.value) {
            navegacion("",'', ''); 
            $('#sideNav').html("");
            firebase.auth().signOut();
        }
      })


}