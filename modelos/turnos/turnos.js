
listadoTurnos = [];
turnosActivos = [];
turnoActual = {}
cuadraturaJornada = {};
function verCuadraturaJornada() {
    balanceJornadaManual = cuadraturaJornada.recaudaciones - cuadraturaJornada.premios
    balanceJornadaContador = cuadraturaJornada.contadoresIn - cuadraturaJornada.contadoresOut
    gananciaMenosGasto = balanceJornadaContador - cuadraturaJornada.gastos
    efectivoEnCaja = gananciaMenosGasto + cuadraturaJornada.cajaBase - cuadraturaJornada.retiros
    diferencia = cuadraturaJornada.entregaFinal - efectivoEnCaja
    ganancia = gananciaMenosGasto - diferencia;
    $('#montoCajaBase').html(puntos(cuadraturaJornada.cajaBase))
    $('#montoRetiro').html(puntos(cuadraturaJornada.retiros))
    $('#montoEntregaFinal').html(puntos(cuadraturaJornada.entregaFinal))
    $('#recaudacionesCuadratura').html(puntos(cuadraturaJornada.recaudaciones))
    $('#entradaContadores').html(puntos(cuadraturaJornada.contadoresIn))
    $('#premiosCuadratura').html(puntos(cuadraturaJornada.premios))
    $('#salidaContadores').html(puntos(cuadraturaJornada.contadoresOut))
    $('#balanceManual').html(puntos(balanceJornadaManual))
    $('#balanceContadores').html(puntos(balanceJornadaContador))
    pintar("balanceContadores", balanceJornadaContador)
    $('#gastos').html(puntos(cuadraturaJornada.gastos))
    $('#ganancia-gastos').html(puntos(gananciaMenosGasto))
    pintar("ganancia-gastos", gananciaMenosGasto)
    $('#efectivoEnCaja').html(puntos(efectivoEnCaja))
    $('#diferencia').html(puntos(diferencia))
    pintar("diferencia", diferencia)
    $('#ganancia').html(puntos(ganancia));
    pintar("ganancia", ganancia)

}

function pintar(objeto, valor) {
    if (parseInt(valor) < 0) {
        $("#" + objeto).attr("class", "text-danger")
    } else {
        $("#" + objeto).attr("class", "text-success")
    }
}

jornadaActual = db.collection("jornadas/" + localStorage.jornadaActual + "/cuadratura/")
    .onSnapshot(function (querySnapshot) {
        cuadraturaJornada = {};
        querySnapshot.forEach(function (doc) {
            cuadraturaJornada = doc.data()
        })
        verCuadraturaJornada();

    })
function iniciarCuadraturaTurno() {
    if (localStorage.turnoActual != "") {
        premiosUpdate = db.collection("cuadraturaTurnos/").doc(localStorage.turnoActual)
            .onSnapshot(function (datos) {
                turnoActual = datos.data();
                if (typeof cuadratura == "undefined") {
                } else {
                    cuadratura.cargarCuadratura();
                }
            });
    }
}



function updateCuadraturaTurnos() {
    cajaBaseTurn = parseInt(turnoActual.cajaBase) + listadoJornadas[0].entregaCaja
    premiosTurn = parseInt(turnoActual.premios)
    gastosTurn = parseInt(turnoActual.gastos)
    recTurn = parseInt(turnoActual.recaudaciones)
    retirosturn = parseInt(turnoActual.retiros)
    entregaCaja = parseInt(turnoActual.entregas)
    balance = recTurn - gastosTurn - premiosTurn;
    efectivoEnCaja = cajaBaseTurn + recTurn - gastosTurn - premiosTurn - retirosturn;
    diferencia = entregaCaja - efectivoEnCaja
    var turnoModifi = db.collection("cuadraturaTurnos/").doc(localStorage.turnoActual)
    return turnoModifi.update({
        balance: balance,
        diferencia: diferencia,
        efectivoEnCaja: efectivoEnCaja
    })
}

function cargarContadores() {
    db.collection("listado/contadores/" + localStorage.jornadaActual).onSnapshot(function (querySnapshot) {
        listadoContadores = [];
        totalOut = 0;
        totalIn = 0;
        totPrem = 0;
        totRec = 0;
        querySnapshot.forEach(function (doc) {
            listadoContadores.push({
                id: doc.id,
                numero: doc.data().numero,
                inAyer: doc.data().entrada,
                outAyer: doc.data().salida,
                premios: doc.data().premios,
                recaudaciones: doc.data().recaudaciones,
                inHoy: doc.data().inHoy,
                outHoy: doc.data().outHoy,
                balanceHoy: doc.data().balanceHoy,
                balanceAyer: doc.data().balanceAyer,
                diferenciaIn: doc.data().diferenciaIn,
                diferenciaOut: doc.data().diferenciaOut,
                estado: doc.data().estado,
                balance: doc.data().balance,
                totalIn: doc.data().totalIn,
                totalOut: doc.data().totalOut,
                balanceContadores: doc.data().balanceContadores,
                multiplicador: doc.data().multiplicador
            });
            totalOut += doc.data().totalOut
            totPrem += doc.data().premios;
            totRec += doc.data().recaudaciones;
            totalIn += doc.data().totalIn;

        })
        db.collection("jornadas/" + localStorage.jornadaActual + "/cuadratura/").doc("datos").update({
            premios: totPrem,
            recaudaciones: totRec,
            contadoresIn: totalIn,
            contadoresOut: totalOut,
            balanceContadores: totalIn - totalOut,
        })
    })
}

class Turnos {
    constructor() {
        this.text = ""
        this.num = '';
        this.apertura = '';
        this.cierre = '';
        this.estado = '';
        this.password = ""
        cargarContadores();
        iniciarRutaTurnos();

    }

    cerrar() {


        Swal.fire({
            title: 'Cerrar Turno',
            text: "Esta seguro que desea cerrar el turno?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
        }).then((result) => {
            if (result.value) {
                var turnoMod = db.collection("turnos").doc(localStorage.turnoActual);
                return turnoMod.update({
                    estado: false,
                    horaRecepcion: obtenerHora()
                }).then(function () {
                    var jornadaMod = db.collection("locales").doc(Usuario.local);
                    return jornadaMod.update({
                        entregaCaja: turnoActual.entregas,
                        horaRecepcion: obtenerHora()
                    }).then(function () {
                        turnoActual = {};
                        turnoActual.cajaBase = 0;
                        turnoActual.premios = 0;
                        turnoActual.recaudaciones = 0;
                        turnoActual.entregas = 0;
                        turnoActual.cajaBase = 0;
                        turnoActual.retiros = 0;
                        location.reload();

                    })

                })

            }
        })



    }

    obtenerTurnos() {
        return listadoTurnos;
    }
    cargarCerrado(identificador) {
        Swal.fire({
            title: 'Ingrese la Pass',
            input: 'password',
            inputPlaceholder: 'Ingrese el codigo de seguridad',
            inputAttributes: {
                maxlength: 10,
                autocapitalize: 'off',
                autocorrect: 'off'
            }
        })

        if (this.password) {
            if (password == "3891") {
                localStorage.turnoActual = identificador;
                cargar('app', 'caja', 'principal')
            } else {
                Swal.fire('Acceso Denegado ')
            }
        }
    }

    cargarAbierto(identificador) {
        localStorage.turnoActual = identificador;
        console.log("cargando turno actual")
        iniciarCuadraturaTurno();
        cargar('app', 'caja', 'principal')
    }
    iniciar() {
        turnosActivos = listadoTurnos.filter(function (el) {
            return el.estado == true || el.estado == "true"
        })

        if (turnosActivos < 1) {

            db.collection("turnos/").add({
                apertura: obtenerHora(),
                cierre: "",
                estado: true,
                jornada: listadoJornadas[0].id,
                local: Usuario.local,
                recepcionCaja: listadoJornadas[0].entregaCaja
            }).then(function (docRef) {
                db.collection("cuadraturaTurnos").doc(docRef.id).set({
                    gastos: 0,
                    premios: 0,
                    recaudaciones: 0,
                    cajaBase: 0,
                    retiros: 0,
                    fecha: obtenerFecha(),
                    hora: obtenerHora(),
                    balance: 0,
                    diferencia: 0,
                    entregaCaja: 0,
                    efectivoEnCaja: 0,
                    entregas: 0,
                    recepcion: 0,
                    horaRecepcion: ""
                })

                var localMod = db.collection("locales").doc(Usuario.local);
                return localMod.update({
                    turnos: docRef.id
                })
            })
                .catch(function (error) {
                    console.error("Error adding document: ", error);
                });
        } else {
            Swal.fire(
                'Turno Activo!',
                'Solo puede haber un turno activo a la vez',
                'warning'
            )

        }


    }

}

