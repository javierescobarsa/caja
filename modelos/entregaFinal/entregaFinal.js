



listadoEntregaFinal = [];
rutaCajaBase = "";
tablaEntregaFinal=""
function iniciarRutaEntregaFinal() {
    rutaCajaBase = rutaCajaBase = db.collection("jornadas/" + localStorage.jornadaActual + "/entregaFinal/").onSnapshot(function (querySnapshot) {
        listadoEntregaFinal = [];
        querySnapshot.forEach(function (doc) {
            listadoEntregaFinal.push({
                id: doc.id,
                monto: doc.data().monto,
                hora: doc.data().hora,
                turno: doc.data().turno

            });

        });
  
        dibujarEntregaFinal()
    })
}


function sumarEntregaFinal() {
    acumuladorEntregaFinal = 0;

    listadoEntregaFinal.forEach(function (doc) {
        acumuladorEntregaFinal += parseInt(doc.monto);
    })
    actualizarEntregaFinal(acumuladorEntregaFinal)


}

function actualizarEntregaFinal(acum) {
    console.log("calculando Entrega"+acum)
    var contadorMod = db.collection("jornadas/" + localStorage.jornadaActual + "/cuadratura/").doc('datos');
    return contadorMod.update({
        entregaFinal: acum
    })

}

function dibujarEntregaFinal() {
    tablaEntregaFinal = "";
    numero = 0
    $("#cuerpoEntregaFinal").html("")
console.log("contenido entrega final"+tablaEntregaFinal)
listadoEntregaFinal.forEach(function (doc) {
        numero++;
        if (parseInt(numero) < 10) {
            numero = "0" + numero
        }
       
            tablaEntregaFinal += `
    <tr>
    <td style="width:25%;font-size:140%; font-weigth:700;">
    `+ numero + `
    </td>
    <td style="width:25%;font-size:120%;">`+ puntos(doc.monto) + `</td>
    <td style="width:25%;font-size:120%;">`+ doc.hora + `</td>
    <td style="width:25%;font-size:120%;"><i class="fas fa-trash-alt" onclick="entregaFinal.eliminar('`+ doc.id + `')"></i></td>
    </tr>
    `
    });


    $("#cuerpoEntregaFinal").html(tablaEntregaFinal)
}

class EntregaFinal {
    constructor(loc, jorn) {
        this.local = loc;
        this.jornada = jorn;
        iniciarRutaEntregaFinal(jorn);
    }


    registrar(monto) {

        db.collection("jornadas/" + localStorage.jornadaActual + "/entregaFinal/").add({
            monto: monto,
            fecha: obtenerFecha(),
            hora: obtenerHora(),
            turno: localStorage.turnoActual
        }).then(function () {
            sumarEntregaFinal();
     
        })
$('#montoEntrega').val();


    }


    eliminar(identificador) {
        Swal.fire({
            title: 'Eliminar CajaBase',
            text: "Esta seguro que desea eliminar la entregaFinal?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
        }).then((result) => {
            if (result.value) {
                db.collection("jornadas/" + localStorage.jornadaActual + "/entregaFinal/").doc(identificador).delete().then(function () {

                    sumarEntregaFinal();
                
                }).catch(function (error) {
                });
            }
        })

    }



}