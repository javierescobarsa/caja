maquinaActual = 0;
function actualizarContador(maquina, control) {

    maquinaActual = listadoContadores.filter(function (el) {
        return el.numero == maquina
    })
    premiosTotales = parseInt(maquinaActual[0].premios);
    recaudacionesTotales = parseInt(maquinaActual[0].recaudaciones);
    inHoy = parseInt(maquinaActual[0].inHoy);
    outHoy = parseInt(maquinaActual[0].outHoy);
    inAyer = parseInt(maquinaActual[0].inAyer);
    outAyer = parseInt(maquinaActual[0].outAyer);
    totalIn = inHoy - inAyer;
    totalOut = outHoy - outAyer;
    totalIn=totalIn*parseInt(maquinaActual[0].multiplicador)
    totalOut=totalOut*parseInt(maquinaActual[0].multiplicador)
  
    balanceHoy = totalIn - totalOut;
    diferenciaIn = recaudacionesTotales - totalIn;
    diferenciaOut = premiosTotales - totalOut;
    balanceContadores = balanceHoy;

    if (inHoy == 0 || outHoy == 0) {
        diferenciaIn = 0;
        diferenciaOut = 0;
        balanceContadores = 0;
        totalIn = 0;
        totalOut = 0;
    }

    var contadorMod = db.collection("listado/contadores/" + localStorage.jornadaActual).doc(maquinaActual[0].id);
    return contadorMod.update({
        balance: recaudacionesTotales - premiosTotales,
        totalIn: totalIn,
        totalOut: totalOut,
        balanceHoy: balanceHoy,
        diferenciaIn: diferenciaIn,
        diferenciaOut: diferenciaOut,
        balanceContadores: balanceContadores
    }).then(function () {
        if (control == true) {
            dibujarTablaContadores()
        }

    })


}
function dibujarTablaEntregaFinal() {
    tablaEntregaFinal = "";
    listadoEntrega.forEach(function (doc) {
        numero = doc.numero
        if (parseInt(doc.maquina) < 10) {
            numero = "0" + doc.numero
        }


        tablaEntregaFinal += `
    <tr>
    <td style="width:25%;"> <button type="button" class="btn btn-primary" >
  `+ numero + `
  </button></td>
    <td>`+ puntos(doc.monto) + `</td>
    <td >`+ puntos(doc.hora) + ` </td>
    <td >Eliminar </td>
    </tr>
    `
    });
    $("#cuerpoEntregaFinal").html(tablaEntregaFinal)
}


function dibujarTablaContadores() {
    tablaContadores = "";
    listadoContadores.forEach(function (doc) {
        numero = doc.numero
        if (parseInt(doc.maquina) < 10) {
            numero = "0" + doc.numero
        }


        tablaContadores += `
    <tr>
    <td style="width:25%;"> <button type="button" class="btn btn-primary" >
  `+ numero + `
  </button></td>
    <td>`+ puntos(doc.inAyer) + `</td>
    <td >`+ puntos(doc.outAyer) + ` </td>
    <td class="bg-secondary" onclick="cargar('app','cuadratura','recaudaciones')" >`+ puntuar(doc.diferenciaIn) + ` </td>
    <td class="bg-secondary" onclick="cargar('app','cuadratura','premios')">`+ puntuar(doc.diferenciaOut) + ` </td>
    <td >`+ puntuar(doc.inHoy) + ` </td>
    <td >`+ puntuar(doc.outHoy) + ` </td>
    <td >`+ puntos(doc.totalIn) + ` </td>
    <td >`+ puntos(doc.totalOut) + ` </td>
    <td >`+ puntuar(doc.balanceContadores) + ` </td>
 

    </tr>
    `
    });
    $("#cuerpoContadores").html(tablaContadores)
}

function dibujarResumen() {
    tablaContadores = "";
    listadoContadores.forEach(function (doc) {
        numero = doc.numero
        if (parseInt(doc.maquina) < 10) {
            numero = "0" + doc.numero
        }

        balan = parseInt(doc.recaudaciones) - parseInt(doc.premios)
        tablaContadores += `
    <tr>
    <td style="width:25%;"> <button type="button" class="btn btn-primary" >
  `+ numero + `
  </button></td>
    <td>`+ puntos(doc.recaudaciones) + `</td>
    <td >`+ puntos(doc.premios) + ` </td>
    <td >`+ puntuar(balan) + ` </td>
 

    </tr>
    `
    });
    $("#cuerpoResumen").html(tablaContadores)
}


class Contadores {
    constructor() {
        dibujarTablaContadores()
    }


    registrar(maquinas, entrada, salida) {

        if (maquina.validar(maquinas) == true) {

            Swal.fire({
                type: 'error',
                timer: 2000,
                title: 'Maquina :' + maquinas + ' No encontrada',
            })
            $('#maquinaContador').val('')
        } else {
            maquinaActual = listadoContadores.filter(function (el) {
                return el.numero == maquinas
            })

            var contMod = db.collection("listado/contadores/" + localStorage.jornadaActual).doc(maquinaActual[0].id);
            return contMod.update({
                estado: true,
                inHoy: parseInt(entrada),
                outHoy: parseInt(salida)
            }).then(function (objeto) {
                $('#maquinaContador').val('')
                $('#entradaContador').val('')
                $('#salidaContador').val('')
                actualizarContador(maquinas, true);


            })
        }




    }
}