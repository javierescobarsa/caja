maqLocal=[];
tablaMaquinas = "";
listadoMaquinas=[];
function iniciarRutaMaquinas(){
if(Usuario.tipo=="administrador"){
    db.collection("maquinas").onSnapshot(function (querySnapshot) {
        listadoMaquinas = [];
        querySnapshot.forEach(function (doc) {
            listadoMaquinas.push({
                id: doc.id,
                num: doc.data().num,
                entrada: doc.data().entrada,
                salida: doc.data().salida,
                local: doc.data().local,
                multiplicador: doc.data().multiplicador
            });
        });
        cargarTablaMaquinas();
    })
}else{
    console.log("cargando maquinas")
    db.collection("maquinas").where("idLocal", "==", Usuario.local).onSnapshot(function (querySnapshot) {
        listadoMaquinas = [];
        querySnapshot.forEach(function (doc) {
            console.log("cargando maquinas")
            listadoMaquinas.push({
                id: doc.id,
                num: doc.data().num,
                entrada: doc.data().entrada,
                salida: doc.data().salida,
                multiplicador: doc.data().multiplicador
            });
        });
       
    })  
}
}
class Maquina {
    constructor(num, entrada, salida, balance) {
        this.num = num;
        this.entrada = entrada;
        this.salida = salida;
        this.balance = balance;
        this.maquinaSelect = []
        this.maquinasFiltradas = [];
        //se traen las maquinas de la db
        iniciarRutaMaquinas();   
    }

    obtenerMaquinas() {
        return listadoMaquinas;
    }

    cargarTabla() {
    }

    cargarMaquinaSelect() {
        this.maquinaSelect = this.obtenerMaquinas().filter(function (el) {
            return el.id == localStorage.maquinaSeleccionada;
        })
        $('#numero').val(this.maquinaSelect[0].num)
        $('#multi').val(this.maquinaSelect[0].multiplicador)
        $('#entrada').val(this.maquinaSelect[0].entrada)
        $('#salida').val(this.maquinaSelect[0].salida)
        $('#local').val(this.maquinaSelect[0].local) 
    }

    modificar(entrada, salida, local,multi) {
        Swal.fire({
            title: 'Está seguro?',
            text: "Desea modificar la maquina?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Modificar'
        }).then((result) => {
            if (result.value) {
                this.guardarCambios(entrada, salida, local,multi)
                cargar('app', 'maquinas', 'listado')
            }
        })
    }

    guardarCambios(entrada, salida, local, multi) {
        var maquinaModificada = db.collection("maquinas").doc(localStorage.maquinaSeleccionada);
        return maquinaModificada.update({
            entrada: entrada,
            salida: salida,
            local: local,
            multiplicador:multi
        })
    }

    eliminar() {
        Swal.fire({
            title: 'Eliminar Maquina',
            text: "Desea Eliminar la maquina?",
            type: 'error',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Eliminar'
        }).then((result) => {
            if (result.value) {
                db.collection("maquinas").doc(localStorage.maquinaSeleccionada).delete().then(function () {
                    cargar("app", "maquinas", "listado");
                }).catch(function (error) {
                });
            }
        })
    }

    cargarModificar(id) {
        localStorage.maquinaSeleccionada = id;
        cargar("app", "maquinas", "modificar")
    }

    validar(numero){
     
        this.maquinasFiltradas = listadoMaquinas.filter(function (el) {

            return el.num == numero
        })

        return this.maquinasFiltradas.length < 1   
    }

    validarMaquinas(numero, local) {

        this.maquinasFiltradas = listadoMaquinas.filter(function (el) {

            return el.num == numero && el.local == local
        })

        return this.maquinasFiltradas.length < 1

    }

    agregar(numero, entrada, salida, local, multiplicador) {
        filtroLocales(local)
        if (this.validarMaquinas(numero, local)) {
            entrada = parseInt(entrada)
            salida = parseInt(salida)
            db.collection("maquinas").add({
                num: numero,
                entrada: entrada,
                salida: salida,
                local: local,
                idLocal:localesfiltro[0].id,
                multiplicador:multiplicador
            })
                .then(function (docRef) {
                    console.log("Document written with ID: ", docRef.id);

                    cargar('app', 'maquinas', 'listado')
                })
                .catch(function (error) {
                    console.error("Error adding document: ", error);
                });

        } else {

            Swal.fire({
                title: 'Maquina Registrada',
                text: "Ya existe la Maquina:" + numero + " en el local:" + local,
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ok'
            })
        }
    }


}

