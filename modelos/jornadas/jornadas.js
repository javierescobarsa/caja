

listadoJornadas = [];
jornadasActivas = [];
listadoContadores = [];
class Jornadas {
    constructor() {
        this.Jornadas;
        this.id = ""
        this.entrega=0;
        db.collection("locales/").onSnapshot(function (querySnapshot) {
            listadoJornadas = [];
            querySnapshot.forEach(function (doc) {

                if (Usuario.local == doc.id) {
                    localStorage.jornadaActual = doc.data().jornada;

                    listadoJornadas.push({
                        id: doc.data().jornada,
                        estado: doc.data().estado,
                        entregaCaja: doc.data().entregaCaja,
                        horaRecepcion: doc.data().horaRecepcion

                    });
                }

            });


            jornadasActivas = listadoJornadas.filter(function (el) {
                return el.estado == true || el.estado == "true"
            })

            if (jornadasActivas < 1) {
                cargar('app', 'jornadas', 'inactiva')
            } else {

                cargar('app', 'turnos', 'listado')
            }

        })


    }
    cerrar() {
        this.cerrarLocal();
        this.cerrarJornada();
        var jornadaMod = db.collection("locales").doc(Usuario.local);

        if (typeof turnoActual.entregas == "undefined") {
            this.entrega=0;
        } else {
            this.entrega=turnoActual.entregas;
        }
        return jornadaMod.update({
            entregaCaja: this.entrega ,
            horaRecepcion: obtenerHora()


        }).then(function () {
            console.log("limpiando variables")
            listadoTurnos = [];
            turnosActivos = [];
            turnoActual = {}
            cuadraturaJornada = {};
            location.reload();
        })
    }

    validarJornada() {

        jornadasActivas = listadoJornadas.filter(function (el) {
            return el.estado == true || el.estado == "true"
        })
        if (jornadasActivas < 1) {
            cargar('app', 'jornadas', 'inactiva')
        } else {
            cargar('app', 'turnos', 'listado');
        }
    }

    obtenerTurnos() {
        return listadoTurnos;
    }

    cerrarLocal() {
        var localMod = db.collection("locales").doc(Usuario.local);
        return localMod.update({
            estado: false

        })
    }

    cerrarJornada() {
        var localMod = db.collection("jornadas").doc(this.obtenerJornada(Usuario.local));
        return localMod.update({
            estado: false

        })

    }


    obtenerJornada(local) {
        this.Jornadas = listadoLocales.filter(function (el) {
            return el.id == local
        })
        return this.Jornadas[0].jornada;
    }

    iniciar() {
        db.collection("jornadas").add({
            estado: true,
            fecha: obtenerFecha(),
            hora: obtenerHora(),
            entregaCaja: 0
        })
            .then(function (docRef) {
                db.collection("jornadas/" + docRef.id + "/cuadratura/").doc("datos").set({
                    gastos: 0,
                    premios: 0,
                    recaudaciones: 0,
                    cajaBase: 0,
                    retiros: 0,
                    entregas: 0,
                    contadoresIn: 0,
                    contadoresOut: 0,
                    balanceContadores: 0,
                    diferencia: 0,
                    entregaCaja: 0,
                    ganancia: 0,
                    fecha: obtenerFecha(),
                    hora: obtenerHora(),
                    entregaFinal: 0
                })
                //aqui se debe setear los registros de los contadores
                // se debe mantener el id de la maquina
                //recorrer el arreglo de maquina

                // quedo bueno

                listadoMaquinas.forEach(function (doc) {
                    console.log("multi" + doc.multiplicador)
                    db.collection("listado/contadores/" + docRef.id + "").doc(doc.id).set({
                        numero: doc.num,
                        entrada: doc.entrada,
                        salida: doc.salida,
                        premios: 0,
                        recaudaciones: 0,
                        inHoy: 0,
                        outHoy: 0,
                        balanceHoy: 0,
                        balanceAyer: 0,
                        diferenciaIn: 0,
                        diferenciaOut: 0,
                        estado: false,
                        totalIn: 0,
                        totalOut: 0,
                        balanceContadores: 0,
                        multiplicador: doc.multiplicador
                    })

                })

                var localMod = db.collection("locales").doc(Usuario.local);
                return localMod.update({
                    estado: true,
                    jornada: docRef.id
                })

            })
            .catch(function (error) {
                console.error("Error adding document: ", error);
            });

    }

}
