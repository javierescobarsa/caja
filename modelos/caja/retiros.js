listadoRetiro = [];
rutaRetiros="";
function iniciarRutaRetiro() {
    rutaRetiros=db.collection("jornadas/" + localStorage.jornadaActual + "/retiros/").onSnapshot(function (querySnapshot) {
        listadoRetiro = [];
        querySnapshot.forEach(function (doc) {
            listadoRetiro.push({
                id: doc.id,
                monto: doc.data().monto,
                hora: doc.data().hora,
                turno:doc.data().turno

            });
        });
        dibujarTablaRetiro();
    })
}

function actualizarRetiroTurno(){
    retiroTurno = listadoRetiro.filter(function (el) {
        return el.turno ==  localStorage.turnoActual
    })
    acumRetiroTurno=0;
    retiroTurno.forEach(function (doc) {
        acumRetiroTurno += parseInt(doc.monto);
    })
    retiroUpdate=db.collection("cuadraturaTurnos").doc(localStorage.turnoActual);
    return retiroUpdate.update({
        retiros: acumRetiroTurno
    }).then(function(){
        updateCuadraturaTurnos();
    })
}

function sumarRetiro() {
    acumuladorRetiro = 0;

    listadoRetiro.forEach(function (doc) {
        acumuladorRetiro += parseInt(doc.monto);
    })
    actualizarRetiro(acumuladorRetiro)


}

function actualizarRetiro(acum) {
console.log("calculando retiros")
    var contadorMod = db.collection("jornadas/" + localStorage.jornadaActual+"/cuadratura/" ).doc('datos');
    return contadorMod.update({
        retiros: acum
    })

}

function dibujarTablaRetiro() {
    tablaRetiro = "";
    numero=0
    listadoRetiro.forEach(function (doc) {
        numero ++;
        if (parseInt(numero) < 10) {
            numero = "0" +numero
        }
        if(localStorage.turnoActual==doc.turno){
        tablaRetiro += `
    <tr>
    <td style="width:25%;font-size:140%; font-weigth:700;">
    `+ numero + `
    </td>
    <td style="width:25%;">`+ puntos(doc.monto)+ `</td>
    <td style="width:25%;">`+ doc.hora + `
    <td style="width:25%;"><i class="fas fa-trash-alt" onclick="retiros.eliminar('`+ doc.id + `')"></i>
</td>
    </tr>
    `}
    });
    $("#cuerpoRetiro").html(tablaRetiro)
}
function dibujarTablaRetiroJornada() {
    tablaRetiro = "";
    numero=0
    listadoRetiro.forEach(function (doc) {
        numero ++;
        if (parseInt(numero) < 10) {
            numero = "0" +numero
        }

        tablaRetiro += `
    <tr>
    <td style="width:25%;font-size:140%; font-weigth:700;">
    `+ numero + `
    </td>
    <td style="width:25%;">`+ puntos(doc.monto)+ `</td>
    <td style="width:25%;">`+ doc.hora + `
    <td style="width:25%;"><i class="fas fa-trash-alt" onclick="retiros.eliminar('`+ doc.id + `')"></i>
</td>
    </tr>
    `
    });
    $("#cuerpoRetiroCuadratura").html(tablaRetiro)
}
class Retiro {
    constructor(loc, jorn) {
        this.local = loc;
        this.jornada = jorn;
        iniciarRutaRetiro(jorn);
    }


    registrar(monto) {

            db.collection("jornadas/" + localStorage.jornadaActual + "/retiros/").add({
                monto: monto,
                fecha: obtenerFecha(),
                hora: obtenerHora(),
                turno:localStorage.turnoActual
            }).then(function(){
                sumarRetiro();
                actualizarRetiroTurno()
            })
            
            $('#montoRetiro').val('').focus()
   
        
    }

  
    eliminar(identificador) {
        Swal.fire({
            title: 'Eliminar Retiro',
            text: "Esta seguro que desea eliminar el premio?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
          }).then((result) => {
            if (result.value) {
                db.collection("jornadas/" +localStorage.jornadaActual + "/retiros/").doc(identificador).delete().then(function () {
                    
                    sumarRetiro();
                    actualizarRetiroTurno()
                }).catch(function (error) {
                });
            }
          })
       
    }



}