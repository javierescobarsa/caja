listadoPremios = [];
premioSeleccionados = [];
filtradoPremios = [];
rutaPremios = "";
acumuladorPrem = 0;
contadorPrem = 0;
AcumPrem = 0;
function iniciarRutaPremios() {

    rutaPremios = db.collection("jornadas/" + localStorage.jornadaActual + "/premios/").orderBy("hora", "desc").onSnapshot(function (querySnapshot) {
        listadoPremios = [];
        querySnapshot.forEach(function (doc) {
            listadoPremios.push({
                id: doc.id,
                monto: doc.data().monto,
                maquina: doc.data().maquina,
                hora: doc.data().hora,
                turno: doc.data().turno

            });



        });
        dibujarPremCuadratura();
        dibujarTablaPremios();
    })
}
function actualizarPremiosTurno() {
    premiosTurno = listadoPremios.filter(function (el) {
        return el.turno == localStorage.turnoActual
    })





    acumPremTurnos = 0;
    premiosTurno.forEach(function (doc) {
        acumPremTurnos += parseInt(doc.monto);
    })
    premiosUpdate = db.collection("cuadraturaTurnos").doc(localStorage.turnoActual);
    return premiosUpdate.update({
        premios: acumPremTurnos
    }).then(function () {
        updateCuadraturaTurnos();
    })
}
function sumarPremios(maquina) {
    acumuladorPremios = 0;
    premioSeleccionados = listadoPremios.filter(function (el) {
        return el.maquina == maquina
    })


    premioSeleccionados.forEach(function (doc) {
        acumuladorPremios += parseInt(doc.monto);
    })
    actualizarPremios(acumuladorPremios, maquina)


}

function actualizarPremios(acum, maquina) {
    maquinaActual = listadoContadores.filter(function (el) {
        return el.numero == maquina
    })
    var contadorMod = db.collection("listado/contadores/" + localStorage.jornadaActual).doc(maquinaActual[0].id);
    return contadorMod.update({
        premios: acum
    }).then(function () {
        actualizarContador(maquina);
        actualizarPremiosTurno();
    })
}

function dibujarTablaPremios() {
    tablaPremios = "";
    contadorPrem=0;
    acumuladorPrem=0;
    listadoPremios.forEach(function (doc) {
        numero = doc.maquina
        if (parseInt(doc.maquina) < 10) {
            numero = "0" + doc.maquina
        }
        if (localStorage.turnoActual == doc.turno) {
            acumuladorPrem += parseInt(doc.monto);
            contadorPrem++;

            tablaPremios += `
    <tr class="bg-ligth">
    <td style="width:25%;font-size:140%; font-weigth:700;">
  `+ numero + `
  </td>
    <td style="width:25%;font-size:120%;">`+ puntos(doc.monto) + `</td>
    <td style="width:25%;font-size:120%;">`+ doc.hora + `
    <td style="width:25%;font-size:120%;"><i class="fas fa-trash-alt" onclick="premios.eliminar('`+ doc.id + `','` + doc.maquina + `')"></i>
</td>
    </tr>
    `

        }
    });
    $('#totalPremios').html(puntos(acumuladorPrem));
    $('#numPremios').html("N°"+contadorPrem);
    $("#cuerpoPremios").html(tablaPremios)
}

function dibujarPremCuadratura() {
    tablaPremios = "";
    contadorPrem=0;
    acumuladorPrem=0;
    listadoPremios.forEach(function (doc) {
        numero = doc.maquina
        if (parseInt(doc.maquina) < 10) {
            numero = "0" + doc.maquina
        }

            acumuladorPrem += parseInt(doc.monto);
            contadorPrem++;

            tablaPremios += `
    <tr class="bg-ligth">
    <td style="width:33%;font-size:140%; font-weigth:700;">
  `+ numero + `
  </td>
    <td style="width:33%;font-size:120%;">`+ puntos(doc.monto) + `</td>
    <td style="width:33%;font-size:120%;">`+ doc.hora + `</td>
    </tr>
    `

    });
    $('#totalPremios').html(puntos(acumuladorPrem));
    $('#numPremios').html("N°"+contadorPrem);
    $("#cuerpoPremiosCuadratura").html(tablaPremios)
}


class Premios {
    constructor(loc, jorn) {
        this.local = loc;
        this.jornada = jorn;
        iniciarRutaPremios(jorn);
        this.montoFiltro = 0;
        this.contadorPremios = 0;
    }


    registrar(monto, maq) {

        if (maquina.validar(maq) == true) {

            Swal.fire({
                type: 'error',
                timer: 2000,
                title: 'Maquina :' + maq + ' No encontrada',
            })
            $('#maquinaPremio').val('').focus()
        } else {
            db.collection("jornadas/" + localStorage.jornadaActual + "/premios/").add({
                maquina: maq,
                monto: monto,
                fecha: obtenerFecha(),
                hora: obtenerHora(),
                turno: localStorage.turnoActual
            }).then(function () {
                sumarPremios(maq)
            })

            $('#montoPremio').val('').focus()
            $('#maquinaPremio').val('')
        }
    }


    eliminar(identificador, maquina) {
        Swal.fire({
            title: 'Eliminar Premio',
            text: "Esta seguro que desea eliminar el premio?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
        }).then((result) => {
            if (result.value) {
                db.collection("jornadas/" + localStorage.jornadaActual + "/premios/").doc(identificador).delete().then(function () {
                    sumarPremios(maquina);
                    actualizarPremiosTurno()

                }).catch(function (error) {
                });
            }
        })

    }

    filtrar(maquina) {
        contadorPrem=0;
        AcumPrem=0;
        if (maquina.length < 1) {
          
            dibujarTablaPremios()
        } else {


            filtradoPremios = listadoPremios.filter(function (el) {
                return el.maquina.startsWith(maquina)
            })


            tablaPremios = "";
            acumuladorPrem = 0;
            contadorPrem = 0;
            filtradoPremios.forEach(function (doc) {
                numero = doc.maquina

                if (parseInt(doc.maquina) < 10) {
                    numero = "0" + doc.maquina
                }
                if (localStorage.turnoActual == doc.turno) {
                    acumuladorPrem += parseInt(doc.monto);
                    contadorPrem++;
                    tablaPremios += `
                    <tr class="bg-ligth">
                    <td style="width:25%;font-size:140%; font-weigth:700;">
                  `+ numero + `
                  </td>
                    <td style="width:25%;font-size:120%;">`+ puntos(doc.monto) + `</td>
                    <td style="width:25%;font-size:120%;">`+ doc.hora + `
                    <td style="width:25%;font-size:120%;"><i class="fas fa-trash-alt" onclick="premios.eliminar('`+ doc.id + `','` + doc.maquina + `')"></i>
                </td>
                    </tr>`

                }
            });
            $('#totalPremios').html(puntos(acumuladorPrem));
            $('#numPremios').html(contadorPrem);
            $("#cuerpoPremios").html(tablaPremios)

        }



    }



}