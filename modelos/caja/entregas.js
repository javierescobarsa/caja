listadoEntrega = [];
rutaEntrega=""
function iniciarRutaEntrega() {
   rutaEntrega= db.collection("jornadas/" + localStorage.jornadaActual + "/entregas/").onSnapshot(function (querySnapshot) {
        listadoEntrega = [];
        querySnapshot.forEach(function (doc) {
            listadoEntrega.push({
                id: doc.id,
                monto: doc.data().monto,
                hora: doc.data().hora,
                turno:doc.data().turno

            });
        });
        dibujarTablaEntrega();
    })
}

function actualizarEntregaTurno(){
    entregas = listadoEntrega.filter(function (el) {
        return el.turno ==  localStorage.turnoActual
    })
    acumEntregaTurno=0;
    entregas.forEach(function (doc) {
        acumEntregaTurno += parseInt(doc.monto);
    })
    entregaUpdate=db.collection("cuadraturaTurnos/").doc(localStorage.turnoActual);
    turnoActual.entregas=acumEntregaTurno;
    return entregaUpdate.update({
        entregas: acumEntregaTurno
    }).then(function(){
        updateCuadraturaTurnos();
    })
}

function sumarEntrega() {
    acumuladorEntrega = 0;
    listadoEntrega.forEach(function (doc) {
        acumuladorEntrega += parseInt(doc.monto);
    })
    actualizarEntrega(acumuladorEntrega)
}

function actualizarEntrega(acum) {
console.log("calculando entregas")
    var contadorMod = db.collection("jornadas/" + localStorage.jornadaActual+"/cuadratura/" ).doc('datos');
    return contadorMod.update({
        entregas: acum
    })

}

function dibujarTablaEntrega() {
    tablaEntrega = "";
    numero=0
    listadoEntrega.forEach(function (doc) {
        numero ++;
        if (parseInt(numero) < 10) {
            numero = "0" +numero
        }
if(localStorage.turnoActual==doc.turno){
    tablaEntrega += `
    <tr>
    <td style="width:25%;font-size:140%; font-weigth:700;">
    `+ numero + `
    </td>
    <td style="width:25%;">`+ puntos(doc.monto)+ `</td>
    <td style="width:25%;">`+ doc.hora + `
    <td style="width:25%;"><i class="fas fa-trash-alt" onclick="entrega.eliminar('`+ doc.id + `')"></i>
    </td>
    </tr>
    `
}
    });
    $("#cuerpoEntrega").html(tablaEntrega)
}

class Entrega {
    constructor(loc, jorn) {
        this.local = loc;
        this.jornada = jorn;
        iniciarRutaEntrega(jorn);
    }
    registrar(monto) {
            db.collection("jornadas/" + localStorage.jornadaActual + "/entregas/").add({
                monto: monto,
                fecha: obtenerFecha(),
                hora: obtenerHora(),
                turno:localStorage.turnoActual
            }).then(function(){
                sumarEntrega();
                actualizarEntregaTurno()
            })
            $('#montoEntrega').val('').focus()
    }

  
    eliminar(identificador) {
        Swal.fire({
            title: 'Eliminar Entrega',
            text: "Esta seguro que desea eliminar el premio?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
          }).then((result) => {
            if (result.value) {
                db.collection("jornadas/" +localStorage.jornadaActual + "/entregas/").doc(identificador).delete().then(function () {
                    
                    sumarEntrega();
                    actualizarEntregaTurno()
                }).catch(function (error) {
                });
            }
          })
       
    }



}