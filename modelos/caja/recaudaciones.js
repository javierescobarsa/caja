listadoRecaudaciones = [];
recaudacionSeleccionados = [];
filtradoRecaudaciones = [];
rutaRecaudaciones=""
function iniciarRutaRecaudaciones() {
    rutaRecaudaciones=db.collection("jornadas/" + localStorage.jornadaActual + "/recaudaciones/").onSnapshot(function (querySnapshot) {
        listadoRecaudaciones = [];
        querySnapshot.forEach(function (doc) {
            listadoRecaudaciones.push({
                id: doc.id,
                monto: doc.data().monto,
                maquina: doc.data().maquina,
                hora: doc.data().hora,
                turno:doc.data().turno
            });
        });
        recaudacionesCuadratura()
        dibujarTablaRecaudaciones();
    })
}

function sumarRecaudaciones(maquina) {
    acumuladorRecaudaciones = 0;
    recaudacionSeleccionados = listadoRecaudaciones.filter(function (el) {
        return el.maquina == maquina
    })
    recaudacionSeleccionados.forEach(function (doc) {
        acumuladorRecaudaciones += parseInt(doc.monto);
    })
    actualizarRecaudaciones(acumuladorRecaudaciones, maquina)

}
function actualizarRecTurno(){
    recaudacionesturno = listadoRecaudaciones.filter(function (el) {
        return el.turno ==  localStorage.turnoActual
    })
    acumRecTurn=0;
    recaudacionesturno.forEach(function (doc) {
        acumRecTurn += parseInt(doc.monto);
    })
    recUpdate=db.collection("cuadraturaTurnos").doc(localStorage.turnoActual);
    return recUpdate.update({
        recaudaciones: acumRecTurn
    }).then(function(){
        updateCuadraturaTurnos();
    })
}

function actualizarRecaudaciones(acum, maquina) {
    maquinaActual = listadoContadores.filter(function (el) {
        return el.numero == maquina
    })
    var contadorMod = db.collection("listado/contadores/" + localStorage.jornadaActual).doc(maquinaActual[0].id);
    return contadorMod.update({
        recaudaciones: acum

    }).then(function () {

        actualizarContador(maquina);
        actualizarRecTurno();
    })


}

function  recaudacionesCuadratura() {
    tablaRecaudaciones = "";
    Nrec=0;
    totRec=0;
    listadoRecaudaciones.forEach(function (doc) {
        numero = doc.maquina
        if (parseInt(doc.maquina) < 10) {
            numero = "0" + doc.maquina
        }
            Nrec++
            totRec+=parseInt(doc.monto);
            tablaRecaudaciones += `
    <tr class="bg-ligth">
    <td style="width:33%;font-size:140%; font-weigth:700;">
  `+ numero + `
  </td>
    <td style="width:33%;font-size:120%;">`+ puntos(doc.monto) + `</td>
    <td style="width:33%;font-size:120%;">`+ doc.hora + `</td>
    </tr>
    `

    });



$('#totalRecaudaciones').html(puntos(totRec));
$('#numRecaudaciones').html("N°"+Nrec);

    $("#cuerpoRecaudacionesCuadratura").html(tablaRecaudaciones)
}
function dibujarTablaRecaudaciones() {
    tablaRecaudaciones = "";
    Nrec=0;
    totRec=0;
    listadoRecaudaciones.forEach(function (doc) {
        numero = doc.maquina
        if (parseInt(doc.maquina) < 10) {
            numero = "0" + doc.maquina
        }
    
        if(localStorage.turnoActual==doc.turno){
            Nrec++
            totRec+=parseInt(doc.monto);
            tablaRecaudaciones += `
    <tr class="bg-ligth">
    <td style="width:25%;font-size:140%; font-weigth:700;">
  `+ numero + `
  </td>
    <td style="width:25%;font-size:120%;">`+ puntos(doc.monto) + `</td>
    <td style="width:25%;font-size:120%;">`+ doc.hora + `
    <td style="width:25%;font-size:120%;"><i class="fas fa-trash-alt" onclick="recaudaciones.eliminar('`+ doc.id + `','` + doc.maquina + `')"></i>
</td>
    </tr>
    `
}
    });



$('#totalRecaudaciones').html(puntos(totRec));
$('#numRecaudaciones').html("N°"+Nrec);

    $("#cuerpoRecaudaciones").html(tablaRecaudaciones)
}

class Recaudaciones {
    constructor() {
        this.local = '';
        this.jornada = '';
        iniciarRutaRecaudaciones();
    }


    registrar(monto, maq) {
        if (maquina.validar(maq) == true) {
            Swal.fire({
                type: 'error',
                timer: 2000,
                title: 'Maquina :' + maq + ' No encontrada',
            })
            $('#maquinaRecaudacion').val('').focus()
        } else {
            db.collection("jornadas/" + localStorage.jornadaActual + "/recaudaciones/").add({
                maquina: maq,
                monto: monto,
                fecha: obtenerFecha(),
                hora: obtenerHora(),
                turno:localStorage.turnoActual
            }).then(function(){
                sumarRecaudaciones(maq)
            })
         
            $('#montoRecaudacion').val('').focus()
            $('#maquinaRecaudacion').val('')
        }


    }


    eliminar(identificador, maquina) {

        Swal.fire({
            title: 'Eliminar Recaudacion',
            text: "Esta seguro que desea eliminar la Recaudacion?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
        }).then((result) => {
            if (result.value) {
                db.collection("jornadas/" + localStorage.jornadaActual + "/recaudaciones/").doc(identificador).delete().then(function () {
                    actualizarContador(maquina);
                    actualizarRecTurno();
                }).catch(function (error) {
                });
            }
        })


    }

    filtrar(maquina) {

        if (maquina.length < 1) {
            dibujarTablaRecaudaciones()
        } else {


            filtradoRecaudaciones = listadoRecaudaciones.filter(function (el) {
                return el.maquina.startsWith(maquina)
            })


            tablaRecaudaciones = "";
            filtradoRecaudaciones.forEach(function (doc) {
                numero = doc.maquina
                if (parseInt(doc.maquina) < 10) {
                    numero = "0" + doc.maquina
                }
                if (localStorage.turnoActual == doc.turno) {
                    Nrec++
                    totRec+=parseIntdoc.monto();
                    tablaRecaudaciones += `
                    <tr class="bg-ligth">
                    <td style="width:25%;font-size:140%; font-weigth:700;">
                  `+ numero + `
                  </td>
                    <td style="width:25%;font-size:120%;">`+ puntos(doc.monto) + `</td>
                    <td style="width:25%;font-size:120%;">`+ doc.hora + `
                    <td style="width:25%;font-size:120%;"><i class="fas fa-trash-alt" onclick="recaudaciones.eliminar('`+ doc.id + `','` + doc.maquina + `')"></i>
                </td>
                    </tr>
                    `
}
            });


            $('#totalRecaudaciones').html(puntos(totRec));
            $('#numRecaudaciones').html("N°"+Nrec);
    
            $("#cuerpoRecaudaciones").html(tablaRecaudaciones)

        }



    }



}