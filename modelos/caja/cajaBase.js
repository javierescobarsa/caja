listadoCajaBase = [];
rutaCajaBase = "";
function iniciarRutaCajaBase() {
    rutaCajaBase = rutaCajaBase = db.collection("jornadas/" + localStorage.jornadaActual + "/cajaBase/").onSnapshot(function (querySnapshot) {
        listadoCajaBase = [];
        querySnapshot.forEach(function (doc) {
            listadoCajaBase.push({
                id: doc.id,
                monto: doc.data().monto,
                hora: doc.data().hora,
                turno: doc.data().turno

            });

        });
        dibujarTablaCajaBase();
    })
}

function actualizarCajaBaseTurno() {
    cajaBaseTurno = listadoCajaBase.filter(function (el) {
        return el.turno == localStorage.turnoActual
    })
    acumCajaBaseTurno = 0;
    cajaBaseTurno.forEach(function (doc) {
        acumCajaBaseTurno += parseInt(doc.monto);
    })
    cajaBaseUpdate = db.collection("cuadraturaTurnos").doc(localStorage.turnoActual);
    return cajaBaseUpdate.update({
        cajaBase: acumCajaBaseTurno
    }).then(function () {
        updateCuadraturaTurnos();
    })
}

function sumarCajaBase() {
    acumuladorCajaBase = 0;

    listadoCajaBase.forEach(function (doc) {
        acumuladorCajaBase += parseInt(doc.monto);
    })
    actualizarCajaBase(acumuladorCajaBase)


}

function actualizarCajaBase(acum) {
    console.log("calculando cajaBase")
    var contadorMod = db.collection("jornadas/" + localStorage.jornadaActual + "/cuadratura/").doc('datos');
    return contadorMod.update({
        cajaBase: acum
    })

}

function dibujarCajaBaseCuadratura() {
    tablaCajaBase = "";
    numero = 0
 

    listadoCajaBase.forEach(function (doc) {
        numero++;
        if (parseInt(numero) < 10) {
            numero = "0" + numero
        }

            tablaCajaBase += `
    <tr>
    <td style="width:25%;font-size:140%; font-weigth:700;">
    `+ numero + `
    </td>
    <td style="width:25%;font-size:120%;">`+ puntos(doc.monto) + `</td>
    <td style="width:25%;font-size:120%;">`+ doc.hora + `</td>
    <td style="width:25%;font-size:120%;"><i class="fas fa-trash-alt" onclick="cajaBase.eliminar('`+ doc.id + `')"></i></td>
    </tr>
    `
    });
    $("#cuerpoCajaBaseCuadratura").html(tablaCajaBase)
}

function dibujarTablaCajaBase() {
    tablaCajaBase = "";
    numero = 0
    if (listadoJornadas[0].entregaCaja > 0) {
        tablaCajaBase += `
        <tr>
        <td style="width:25%;"> 
     EfectivoTurnoAnterior
      </td>
        <td style="width:25%;">`+ puntos(listadoJornadas[0].entregaCaja) + `</td>
        <td style="width:25%;">`+ listadoJornadas[0].horaRecepcion + `</td>
        <td style="width:25%;"></td>
        </tr>
        
        
        `
    }

    listadoCajaBase.forEach(function (doc) {
        numero++;
        if (parseInt(numero) < 10) {
            numero = "0" + numero
        }
        if (localStorage.turnoActual == doc.turno) {
            tablaCajaBase += `
    <tr>
    <td style="width:25%;font-size:140%; font-weigth:700;">
    `+ numero + `
    </td>
    <td style="width:25%;font-size:120%;">`+ puntos(doc.monto) + `</td>
    <td style="width:25%;font-size:120%;">`+ doc.hora + `</td>
    <td style="width:25%;font-size:120%;"><i class="fas fa-trash-alt" onclick="cajaBase.eliminar('`+ doc.id + `')"></i></td>
    </tr>
    `}
    });
    $("#cuerpoCajaBase").html(tablaCajaBase)
}

class CajaBase {
    constructor(loc, jorn) {
        this.local = loc;
        this.jornada = jorn;
        iniciarRutaCajaBase(jorn);
    }


    registrar(monto) {

        db.collection("jornadas/" + localStorage.jornadaActual + "/cajaBase/").add({
            monto: monto,
            fecha: obtenerFecha(),
            hora: obtenerHora(),
            turno: localStorage.turnoActual
        }).then(function () {
            sumarCajaBase();
            actualizarCajaBaseTurno()
        })
$('#cajaBaseFinal').val();
        $('#montoCajaBase').val('').focus()


    }


    eliminar(identificador) {
        Swal.fire({
            title: 'Eliminar CajaBase',
            text: "Esta seguro que desea eliminar el premio?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
        }).then((result) => {
            if (result.value) {
                db.collection("jornadas/" + localStorage.jornadaActual + "/cajaBase/").doc(identificador).delete().then(function () {

                    sumarCajaBase();
                    actualizarCajaBaseTurno()
                }).catch(function (error) {
                });
            }
        })

    }



}