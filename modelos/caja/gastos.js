listadoGastos = [];
premioSeleccionados = [];
rutaGastos = ""
function iniciarRutaGastos() {
    rutaGastos = db.collection("jornadas/" + localStorage.jornadaActual + "/gastos/").onSnapshot(function (querySnapshot) {
        listadoGastos = [];
        querySnapshot.forEach(function (doc) {
            listadoGastos.push({
                id: doc.id,
                monto: doc.data().monto,
                motivo: doc.data().motivo,
                hora: doc.data().hora,
                turno: doc.data().turno
            });
        });
        dibujarTablaGastos();
        tabGastCuadra();
    })
}

function actualizarGastosTurno() {
    gastosTurno = listadoGastos.filter(function (el) {
        return el.turno == localStorage.turnoActual
    })
    acumGastosTurno = 0;
    gastosTurno.forEach(function (doc) {
        acumGastosTurno += parseInt(doc.monto);
    })
    gastosUpdate = db.collection("cuadraturaTurnos").doc(localStorage.turnoActual);
    return gastosUpdate.update({
        gastos: acumGastosTurno
    }).then(function () {
        updateCuadraturaTurnos();
    })
}

function sumarGastos() {
    acumuladorGastos = 0;

    listadoGastos.forEach(function (doc) {
        acumuladorGastos += parseInt(doc.monto);
    })
    actualizarGastos(acumuladorGastos)


}

function actualizarGastos(acum) {
    var contadorMod = db.collection("jornadas/" + localStorage.jornadaActual + "/cuadratura/").doc('datos');
    return contadorMod.update({
        gastos: acum
    })

}

function tabGastCuadra() {
    tablaGastos = "";
    numero = 0
    listadoGastos.forEach(function (doc) {
        numero++;
        if (parseInt(numero) < 10) {
            numero = "0" + numero
        }

        tablaGastos += `
    <tr>
    <td style="width:20%;font-size:140%; font-weigth:700;">
    `+ numero + `
    </td>
    <td style="width:20%;font-size:120;">`+ puntos(doc.monto) + `</td>
    <td style="width:20%;font-size:120;">`+ doc.motivo + `</td>
    <td style="width:20%;font-size:120;">`+ doc.hora + `</td>
    </tr>
    `
    });
    $("#cuerpoGastosCuadratura").html(tablaGastos)
}
function dibujarTablaGastos() {
    tablaGastos = "";
    numero = 0
    listadoGastos.forEach(function (doc) {
        numero++;
        if (parseInt(numero) < 10) {
            numero = "0" + numero
        }
        if (localStorage.turnoActual == doc.turno) {
            tablaGastos += `
    <tr>
    <td style="width:20%;font-size:140%; font-weigth:700;">
    `+ numero + `
    </td>
    <td style="width:20%;font-size:120;">`+ puntos(doc.monto) + `</td>
    <td style="width:20%;font-size:120;">`+ doc.motivo + `</td>
    <td style="width:20%;font-size:120;">`+ doc.hora + `
    <td style="width:20%;font-size:120;"><i class="fas fa-trash-alt" onclick="gastos.eliminar('`+ doc.id + `','` + doc.maquina + `')"></i>
</td>
    </tr>
    `}
    });
    $("#cuerpoGastos").html(tablaGastos)
}

class Gastos {
    constructor(loc, jorn) {
        this.local = loc;
        this.jornada = jorn;
        iniciarRutaGastos(jorn);
    }
    registrar(monto, motivo) {
        db.collection("jornadas/" + localStorage.jornadaActual + "/gastos/").add({
            motivo: motivo,
            monto: monto,
            fecha: obtenerFecha(),
            hora: obtenerHora(),
            turno: localStorage.turnoActual
        }).then(function () {
            sumarGastos();
            actualizarGastosTurno()
        })
        $('#montoGasto').val('').focus()
        $('#motivoGasto').val('')
    }


    eliminar(identificador, maquina) {
        Swal.fire({
            title: 'Eliminar Premio',
            text: "Esta seguro que desea eliminar el premio?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
        }).then((result) => {
            if (result.value) {
                db.collection("jornadas/" + localStorage.jornadaActual + "/gastos/").doc(identificador).delete().then(function () {

                    sumarGastos();
                    actualizarGastosTurno()
                }).catch(function (error) {
                });
            }
        })

    }



}